# FoundryVTT dnd5e lang fr-FR

EN : 
		This module adds the option to translate in spanish (SPAIN) language Dungeons ands Dragons 5th edition System. 

		Installation
      In the Add-On Modules option click on Install Module and place the following link in the field Manifest URL
      https://gitlab.com/xosemarcote/dnd5_es_classes/-/raw/master/module.json
      Once this is done, enable the module in the settings of the world in which you intend to use it and then change the language in the settings.
ES : 
		Este módulo añade la opción de traducir al castellano (ESPAÑA) el sistema Dungeons and Dragons 5e. 
		
		Instalación
			En la opción Add-On Modules pincha be Install Module e inserta el siguiente enlace en el campo Manifest URL
		  https://gitlab.com/xosemarcote/dnd5_es_classes/-/raw/master/module.json  
			Una vez hecho esto, habilita este módulo en las opciónes del mundo en que quieras usarlo, entonces será cargado el idioma en las opciones.	
